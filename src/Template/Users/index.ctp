<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Register'), ['controller' =>'users' ,'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Login'), ['controller' =>'users' ,'action' => 'login']) ?></li>
    </ul>
</nav>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Index') ?></h3>
    <p>Welcome Security Guru!<br/> Please <?= $this->Html->link(__('(LogIn)'), ['controller' =>'users' ,'action' => 'login']) ?> or <?= $this->Html->link(__('(Register)'), ['controller' =>'users' ,'action' => 'add']) ?></p>
</div>
