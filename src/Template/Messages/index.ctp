<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Message[]|\Cake\Collection\CollectionInterface $messages
 */
use Cake\Utility\Security;
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Message'), ['controller' => 'Messages', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Messages List'), ['controller' => 'Messages', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="messages index large-9 medium-8 columns content">
    <h3><?= __('Messages') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('from_id','From') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Date Sent') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Message') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($messages as $message): ?>

            <tr>
                <td><?= $message->has('user') ? h($message->user->username) : '' ?></td>
                <td><?= h($message->created) ?></td>
                <td><?php
                    $decryptedBody = "test";
                    if (base64_decode($message->body)) {
                        $decryptedBody = Security::decrypt(base64_decode($message->body), '12345678901234567890123456789012');
                        $message->body = $decryptedBody;
                    }
                    echo h($message->body)

                    ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
