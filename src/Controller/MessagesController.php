<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Security;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages
 *
 * @method \App\Model\Entity\Message[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MessagesController extends AppController
{

   /* public function beforeFilter() {
        //$this->Auth->deny(['index', 'view']);

    }*/
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users'],
            //'fields' => ['Messages.body', 'Messages.created', 'Users.username'],
            'conditions' => [
                'Messages.to_id' => $this->Auth->user('id'),
            ],
            'order' => [
                'Messages.created' => 'desc',
            ],
            'limit' => 25
        ];
        $messages = $this->paginate($this->Messages);
        $this->set(compact('messages'));
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $message = $this->Messages->newEntity();
        if ($this->request->is('post')) {
            $message = $this->Messages->patchEntity($message, $this->request->getData());
            $encryptedBody = base64_encode(Security::encrypt($message->body, '12345678901234567890123456789012'));
            //$decryptedBody = Security::decrypt(base64_decode($encryptedBody), '12345678901234567890123456789012');
            $message->body = $encryptedBody;
            $message->from_id = $this->getRequest()->getSession()->read('Auth.User.id');

            if ($this->Messages->save($message)) {

                $this->Flash->success(__('The message has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The message could not be saved. Please, try again.'));
        }
        $users = $this->Messages->Users->find('list', [
            'limit' => 200,
            'conditions' => ['Users.id !=' => $this->getRequest()->getSession()->read('Auth.User.id')]
        ]);
        $this->set(compact('message', 'users'));
    }

}
